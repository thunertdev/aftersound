import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {StyledLayout} from "../styles/styledLayout";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Aftersound Göttingen</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <StyledLayout >
        <video
            loop
            muted
            autoplay=""
            controls
            webkit-playsinline=""
            src={require('../public/Opener-website.mp4')}
        />
      </StyledLayout>
    </div>
  )
}

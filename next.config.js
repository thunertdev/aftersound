const { nextI18NextRewrites } = require('next-i18next/rewrites')
const withReactSvg = require('next-react-svg')
const path = require('path')
const localeSubpaths = {}
const withVideos = require('next-videos')

// eslint-disable-next-line no-undef
module.exports = {
    rewrites: async () => nextI18NextRewrites(localeSubpaths),
    publicRuntimeConfig: {
        localeSubpaths,
    },
}

// eslint-disable-next-line no-undef
module.exports = withReactSvg({
    include: path.resolve(__dirname, 'public'),
    webpack(config) {
        return config
    }
})

// eslint-disable-next-line no-undef
module.exports = withVideos()
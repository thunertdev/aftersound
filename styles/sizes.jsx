const size = {
    mobile: '576px',
    tablet: '768px',
    laptop: '992px',
    desktop: '1200px'
}

export const downMobile = `(max-width: ${size.mobile})`;
export const downTablet = `(max-width: ${size.tablet})`;
export const upTablet = `(min-width: ${size.tablet})`;

import styled from 'styled-components';

const StyledLayout = styled.div`
  width: 100%;
  height: 100vh;
  
  video {
    width: 100%;
    height: 100%;
  }
`;

export { StyledLayout };
const NextI18Next = require('next-i18next').default
const path = require('path')

module.exports = new NextI18Next({
    otherLanguages: ['de', 'en'],
    browserLanguageDetection: true,
    defaultLanguage: 'de',
    defaultNS: 'footer',
    localePath: path.resolve('./public/static/locales')
})